# Asset Tracking System

![image](https://gitlab.com/PkSai/asset-tracking-system/-/raw/main/asset1.png)


A Hyperledger Fabric based Asset Tracking System. Tracks various assets for organization. Items can be tagged with certain features for easier tracking.

# Features
- **Identification**
- **Name**
- **Description**
- **Location**
- **Storage**
- **Appraised Value**

**Built with Gin and chaincode written in Go**

