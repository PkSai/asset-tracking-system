package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/gateway"
)

type Asset struct {
	ID             string `json:"ID"`
	Name           string `json:"name"`
	Description    string `json:"description"`
	VirtualSpace   int    `json:"virtualSpace"`
	Owner          string `json:"owner"`
	AppraisedValue int    `json:"appraisedValue"`
}

func main() {
	// Registering User, activating chaincode the chaincode path and so on
	os.Setenv("DISCOVERY_AS_LOCALHOST", "true")
	wallet, err := gateway.NewFileSystemWallet("wallet")
	if err != nil {
		fmt.Printf("Failed to create wallet: %s\n", err)
		os.Exit(1)
	}

	if !wallet.Exists("appUser") {
		err = populateWallet(wallet)
		if err != nil {
			fmt.Printf("Failed to populate wallet contents: %s\n", err)
			os.Exit(1)
		}
	}

	ccpPath := filepath.Join(
		"..",
		"..",
		"test-network",
		"organizations",
		"peerOrganizations",
		"org1.example.com",
		"connection-org1.yaml",
	)

	gw, err := gateway.Connect(
		gateway.WithConfig(config.FromFile(filepath.Clean(ccpPath))),
		gateway.WithIdentity(wallet, "appUser"),
	)
	if err != nil {
		fmt.Printf("Failed to connect to gateway: %s\n", err)
		os.Exit(1)
	}
	defer gw.Close()

	network, err := gw.GetNetwork("mychannel")
	if err != nil {
		fmt.Printf("Failed to get network: %s\n", err)
		os.Exit(1)
	}

	contract := network.GetContract("atcc")

	// Getting all the assets at once
	// Calls the GetAllAssets API
	// Routes the connection to the assets webpage
	result, err := contract.EvaluateTransaction("GetAllAssets")
	if err != nil {
		fmt.Printf("Failed to evaluate transaction: %s\n", err)
		os.Exit(1)
	}
	fmt.Println(result)
	getAllAssets := func(c *gin.Context) {
		result, err := contract.EvaluateTransaction("GetAllAssets")
		if err != nil {
			fmt.Printf("Failed to evaluate transaction: %s\n", err)
			os.Exit(1)
		}

		asset_final := []Asset{}
		_ = json.Unmarshal([]byte(result), &asset_final)
		c.HTML(http.StatusOK, "asset.html", gin.H{"asset_final": asset_final})
	}

	// Creating a new Asset
	// Gets the new asset page to fill in data
	// Routed data is binded and sent to the Create Asset API
	// Redirect to assets page
	assetNewGetHandler := func(c *gin.Context) {
		c.HTML(http.StatusOK, "newAsset.html", gin.H{})
	}
	assetNewPostHandler := func(c *gin.Context) {
		asset_post := &Asset{}
		if err := c.Bind(asset_post); err != nil {
			return
		}
		if asset_post.ID == "" || asset_post.Name == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		result, err = contract.SubmitTransaction("CreateAsset", asset_post.ID, asset_post.Name, asset_post.Description, strconv.FormatInt(int64(asset_post.VirtualSpace), 10), asset_post.Owner, strconv.FormatInt(int64(asset_post.AppraisedValue), 10))
		if err != nil {
			fmt.Printf("Failed to submit transaction: %s\n", err)
			os.Exit(1)
		}
		c.Redirect(http.StatusMovedPermanently, "/")
	}

	// Update an existing asset
	// Same deal as create asset
	assetUpdateGetHandler := func(c *gin.Context) {
		c.HTML(http.StatusOK, "updateAsset.html", gin.H{})
	}
	assetUpdatePostHandler := func(c *gin.Context) {
		asset_update := &Asset{}
		if err := c.Bind(asset_update); err != nil {
			return
		}
		if asset_update.ID == "" || asset_update.Name == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		_, err = contract.SubmitTransaction("UpdateAsset", asset_update.ID, asset_update.Name, asset_update.Description, strconv.FormatInt(int64(asset_update.VirtualSpace), 10), asset_update.Owner, strconv.FormatInt(int64(asset_update.AppraisedValue), 10))
		if err != nil {
			fmt.Printf("Failed to submit transaction: %s\n", err)
			os.Exit(1)
		}

		c.Redirect(http.StatusFound, "/assets")
	}

	// Reading a single asset
	// Directs to a page where the ID needs to be given
	// Then redirects to the list single asset page
	readAssetGetHandler := func(c *gin.Context) {
		c.HTML(http.StatusOK, "readAsset.html", gin.H{})
	}
	readAssetPostHandler := func(c *gin.Context) {
		asset_read := &Asset{}
		if err := c.Bind(asset_read); err != nil {
			return
		}
		if asset_read.ID == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		result, err := contract.EvaluateTransaction("ReadAsset", asset_read.ID)
		if err != nil {
			fmt.Printf("Failed to evaluate transaction: %s\n", err)
			os.Exit(1)
		}
		_ = json.Unmarshal([]byte(result), &asset_read)
		c.HTML(http.StatusOK, "singleAsset.html", gin.H{"asset_read": asset_read})
	}

	// To delete the asset
	deleteAssetGetHandler := func(c *gin.Context) {
		c.HTML(http.StatusOK, "deleteAsset.html", gin.H{})
	}
	deleteAssetPostHandler := func(c *gin.Context) {
		asset_del := &Asset{}
		if err := c.Bind(asset_del); err != nil {
			return
		}
		if asset_del.ID == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		_, err := contract.EvaluateTransaction("DeleteAsset", asset_del.ID)
		if err != nil {
			fmt.Printf("Failed to evaluate transaction: %s\n", err)
			os.Exit(1)
		}
		fmt.Println(asset_del)
		c.HTML(http.StatusOK, "assetDeleted.html", gin.H{"asset_del": asset_del})
	}

	// Index, landing page
	getIndex := func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})
	}

	// Web server stuff
	router := gin.Default()
	router.LoadHTMLGlob("template/*.html")
	router.Static("/css", "template/css") // this is loading css file
	router.Static("/img", "template/img")
	router.GET("/", getIndex)
	router.GET("/assets", getAllAssets)
	router.GET("/asset/read", readAssetGetHandler)
	router.POST("/asset/read", readAssetPostHandler)
	router.GET("/asset/new", assetNewGetHandler)
	router.POST("/asset/new", assetNewPostHandler)
	router.GET("/asset/update", assetUpdateGetHandler)
	router.POST("/asset/update", assetUpdatePostHandler)
	router.GET("/asset/delete", deleteAssetGetHandler)
	router.POST("/asset/delete", deleteAssetPostHandler)
	router.Run("localhost:3000")

}

func populateWallet(wallet *gateway.Wallet) error {
	credPath := filepath.Join(
		"..",
		"..",
		"test-network",
		"organizations",
		"peerOrganizations",
		"org1.example.com",
		"users",
		"User1@org1.example.com",
		"msp",
	)

	certPath := filepath.Join(credPath, "signcerts", "cert.pem")
	// read the certificate pem
	cert, err := ioutil.ReadFile(filepath.Clean(certPath))
	if err != nil {
		return err
	}

	keyDir := filepath.Join(credPath, "keystore")
	// there's a single file in this dir containing the private key
	files, err := ioutil.ReadDir(keyDir)
	if err != nil {
		return err
	}
	if len(files) != 1 {
		return errors.New("keystore folder should have contain one file")
	}
	keyPath := filepath.Join(keyDir, files[0].Name())
	key, err := ioutil.ReadFile(filepath.Clean(keyPath))
	if err != nil {
		return err
	}

	identity := gateway.NewX509Identity("Org1MSP", string(cert), string(key))

	err = wallet.Put("appUser", identity)
	if err != nil {
		return err
	}
	return nil
}
